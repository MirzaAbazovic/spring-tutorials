package ba.programiraj.security.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class Account {
    private String id;
    private String number;
    private BigDecimal currentAmount;
}
