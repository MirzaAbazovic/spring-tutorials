package ba.programiraj.security.api;

import ba.programiraj.security.model.Account;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("api")
public class AccountController {

    @GetMapping("ping")
    public String pingPong(){
        return "pong";
    }

    @GetMapping("accounts")
    public List<Account> getAccounts(){
        Account account = Account.builder()
                .id("1")
                .currentAmount(BigDecimal.valueOf(12))
                .number("AD234234DAFAS333")
                .build();
        return Collections.singletonList(account);
    }
}
