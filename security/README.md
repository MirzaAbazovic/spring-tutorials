# Spring security
[HOME](../README.md)

## Initialise spring security
 
By adding dep.

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-security</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-test</artifactId>
    <scope>test</scope>
</dependency>
```
Web app is secured by default and all request are secured by chain of filters.
Default credentials:
U:user 
P: generated and displayed in output when starting app

```shell
2022-11-06T12:37:40.080+01:00  WARN 10047 --- [  restartedMain] .s.s.UserDetailsServiceAutoConfiguration : 

Using generated security password: 485a45e6-6140-4e34-88ce-929c7d5bb4b5

This generated password is for development use only. Your security configuration must be updated before running your application in production.

2022-11-06T12:37:40.173+01:00  INFO 10047 --- [  restartedMain] o.s.s.web.DefaultSecurityFilterChain     : 
Will secure any request with [
org.springframework.security.web.session.DisableEncodeUrlFilter@535ef059, 
org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter@1a70543f, 
org.springframework.security.web.context.SecurityContextHolderFilter@1d3b8dba, 
org.springframework.security.web.header.HeaderWriterFilter@36a46cfe, 
org.springframework.security.web.csrf.CsrfFilter@e03482, 
org.springframework.security.web.authentication.logout.LogoutFilter@33da37a3, 
org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter@357e9740, 
org.springframework.security.web.authentication.ui.DefaultLoginPageGeneratingFilter@7ef4effd, 
org.springframework.security.web.authentication.ui.DefaultLogoutPageGeneratingFilter@24aaa36b, 
org.springframework.security.web.authentication.www.BasicAuthenticationFilter@22ad6944, 
org.springframework.security.web.savedrequest.RequestCacheAwareFilter@768bff72, 
org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter@7abc4aad, 
org.springframework.security.web.authentication.AnonymousAuthenticationFilter@5b0ba318, 
org.springframework.security.web.access.ExceptionTranslationFilter@68e7b4fa, 
org.springframework.security.web.access.intercept.AuthorizationFilter@6a583c5
]
```

You get login page from DefaultLoginPageGeneratingFilter on /login, 
and logout from DefaultLogoutPageGeneratingFilter on /logout.

To change default user and pass use config.

```shell
spring.security.user.name=customUsername
spring.security.user.password=customPassword
```

You can also test security

```java
@WebMvcTest
class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity()).build();
    }


    @Test
    void pingPongUnauthorized() throws Exception {
        mockMvc.perform(get("/api/ping"))
                .andExpect(status().isUnauthorized());
    }

    @WithMockUser(value = "testUser")
    @Test
    void pingPongAuthorized() throws Exception {
        mockMvc.perform(get("/api/ping"))
                .andExpect(status().isOk());
    }
}
```

or simplified with annotations

```java
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void pingPongUnauthorized() throws Exception {
        mockMvc.perform(get("/api/ping"))
                .andExpect(status().isUnauthorized());
    }

    @WithMockUser(value = "testUser")
    @Test
    void pingPongAuthorized() throws Exception {
        mockMvc.perform(get("/api/ping"))
                .andExpect(status().isOk());
    }
}
```

## Architecture
In this image are all important "players" in Spring security and flow of authorisation:

![Spring Security Architecture](doc/img/Spring-Security-Architecture.png)

![Spring Security Flow](doc/img/spring-security-flow.png)

We can extend serurity by providing your implementations. Let us first implement in memory user by providing:
- UserDetailsService
- PasswordEncoder

```java
@Configuration
public class SecurityConfig {

    @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager userDetailsManager = new InMemoryUserDetailsManager();
        UserDetails userMirza = User.withUsername("mirza")
                .password("lozinka")
                .authorities("admin")//at least one authority is mandatory
                .build();
        userDetailsManager.createUser(userMirza);
        return userDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

}
```

Take a look at Spring interfaces/classes:
- UserDetails
- UserDetailsService
- UserDetailsManager
- UserDetailsPasswordService
- InMemoryUserDetailsManager
- PasswordEncoder
- NoOpPasswordEncoder



[HOME](../README.md)

